(function() {
    class Domain {
        constructor(props = {}) {
            this.props = props;

            this.formatPrice = price => (price > 0 ? `$${Math.floor(price / 100)}.${Math.floor(price % 100)}` : 'FREE');

            this.calcTotalFeaturesPrice = () =>
                this.formatPrice(
                    Object.keys(this.props.cart.domain.features)
                        .filter(f => this.props.cart.domain.features[f].active)
                        .reduce((total, feature) => total + this.props.cart.domain.features[feature].price, 0)
                );
        }

        onChange(e) {
            console.log(e.target.value);
        }

        createElement(type, classList, ...children) {
            const element = document.createElement(type);
            classList.forEach(cl => element.classList.add(cl));
            children
                .map(el => (el instanceof Node ? el : document.createTextNode(el)))
                .forEach(el => element.appendChild(el));
            return element;
        }

        render() {
            // const root = this.createElement('section', ['base-dr']);
            // const title = this.createElement('h1', ['domain-registration'], 'Domain Registration');
            // root.appendChild(title);
            // const inBundle = this.createElement('section', ['in-bundle'], this.createElement('span', ['oval-3']));
            // root.appendChild(inBundle);
            // const domainName = this.createElement(
            //     'span',
            //     ['domain-name'],
            //     `${this.props.cart.domain.name}.${this.props.cart.domain.tld}`
            // );
            // const price = this.createElement('span', ['price'], this.formatPrice(this.props.cart.domain.price));
            // const renewalPrice = this.createElement(
            //     'span',
            //     ['renewal-price'],
            //     `Renewal price ${this.formatPrice(this.props.cart.domain.renewalPrice)}`
            // );
            // const mainOptions = this.createElement('section', ['main-options']);
            // inBundle.appendChild(domainName);
            // inBundle.appendChild(price);
            // inBundle.appendChild(renewalPrice);
            //
            // const registrationPeriod = document.createElement('select');
            // registrationPeriod.onchange = this.onChange;
            // registrationPeriod.classList.add('registration-period');
            // for (let i = 1; i <= 5; i++) {
            //     const option = document.createElement('option');
            //     option.innerText = `${i} Year${i > 1 ? 's' : ''}`;
            //     option.value = i;
            //     if (i == this.props.cart.domain.registration) {
            //         option.selected = true;
            //     }
            //     registrationPeriod.appendChild(option);
            // }
            //
            // mainOptions.appendChild(registrationPeriod);
            // inBundle.appendChild(mainOptions);
            //
            //
            // return root;

            return `
              <section class="base-dr">
                <h1 class="domain-registration">Domain Registration</h1>
                <section class="in-bundle">
                  <span class="oval-3"></span>
                  <span class="domain-name">${this.props.cart.domain.name + '.' + this.props.cart.domain.tld}</span>
                  <span class="price">${this.formatPrice(this.props.cart.domain.price)}</span>
                  <span class="renewal-price"></span>
                  <section class="main-options">
                    <select class="registration-period" ${this.props.cart.domain.registration}>
                      <option value="1" ${this.props.cart.domain.registration == 1 ? 'selected' : ''}>1 Year</option>
                      <option value="2" ${this.props.cart.domain.registration == 2 ? 'selected' : ''}>2 Years</option>
                      <option value="3" ${this.props.cart.domain.registration == 3 ? 'selected' : ''} >3 Years</option>
                      <option value="4" ${this.props.cart.domain.registration == 4 ? 'selected' : ''}>4 Years</option>
                      <option value="5" ${this.props.cart.domain.registration == 5 ? 'selected' : ''}>5 Years</option>
                    </select>
                    <span class="autorenew-option"><span class="autorenew-option-title">Auto-renew</span><input type="checkbox" ${this
                        .props.cart.domain.autorenew && 'checked'}></span>
                  </section>
                  <section class="features">
                    <div class="features-total-price">${this.calcTotalFeaturesPrice()}</div>
                    <section class="features-list">
                      <section class="feature">
                        <span class="feature-name">WhoisGuard</span>
                        <span class="feature-price">${this.formatPrice(
                            this.props.cart.domain.features.whoisguard.price
                        )}</span>
                        <input type="checkbox" ${this.props.cart.domain.features.whoisguard.active && 'checked'}>
                      </section>
                      <section class="feature">
                        <span class="feature-name">PremiumDNS</span>
                        <span class="feature-price">${this.formatPrice(
                            this.props.cart.domain.features.premiumDns.price
                        )}</span>
                        <input type="checkbox" ${this.props.cart.domain.features.premiumDns.active && 'checked'}>
                      </section>
                      <section class="feature">
                        <span class="feature-name">Private Email</span>
                        <span class="feature-price">${this.formatPrice(
                            this.props.cart.domain.features.privateEmail.price
                        )}</span>
                        <input type="checkbox" ${this.props.cart.domain.features.privateEmail.active &&
                            'checked'} onchange="new Function(${this.onChange})">
                      </section>
                    </section>
                  </section>
                </section>
              </section>
             `;
        }
    }

    window.nc = window.nc || {};

    window.nc.Domain = Domain;
})();
