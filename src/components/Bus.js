import _ from 'lodash';
import PubSub from './PubSub';

class Bus {
    constructor(cartItems, shoppingCart) {
        this.cartItems = cartItems;
        this.widgetPubSubs = [];
        this.shoppingCart = shoppingCart;

        this._shoppingCartEvent = ({payload, eventType}) => ({
            payload,
            eventType,
            widgetId: null,
            widgetType: 'shoppingCart',
        });
    }

    addWidget({widgetType, cartItemId}) {
        const pubSub = new PubSub(this, widgetType, cartItemId);
        this.widgetPubSubs.push(pubSub);
        return pubSub;
    }

    removeCartItem(id) {
        console.log('remove cart item:', id);
        if (this.cartItems.findIndex(el => el.id === id) === -1) {
            return;
        }
        this.widgetPubSubs = this.widgetPubSubs.filter(pubSub => pubSub.id !== id);
        this.cartItems.splice(this.cartItems.findIndex(el => el.id === id), 1);
        this.cartItems.forEach(item => {
            if (item.relatedItemsIds.includes(id)) {
                item.relatedItemsIds = item.relatedItemsIds.filter(i => i !== id);
            }
        });
        this.shoppingCart.$forceUpdate();
    }

    emit({widgetId, widgetType, payload, eventType}) {
        switch (eventType) {
            case 'CHANGE': {
                const item = this.cartItems.find(item => item.id === widgetId);
                _.set(item.state, payload.path, payload.value);
                console.log('emit', eventType, ' | ', widgetId, ' | ', widgetType, ' | ', JSON.stringify(payload));
            }
        }

        const cartItem = this.cartItems.find(item => item.id === widgetId);

        cartItem.relatedItemsIds.forEach(itemId => {
            const pubSub = this.widgetPubSubs.find(item => item.id === itemId);
            pubSub.handle({widgetId, widgetType, payload, eventType});
        });
    }

    broadcast(eventType, payload) {
        this.widgetPubSubs.forEach(widget => {
            widget.handle(this._shoppingCartEvent({payload, eventType}));
        });
    }

    broadcastForSibblings(event) {
        this.widgetPubSubs
            .filter(pubSub => pubSub.id !== event.widgetId && pubSub.widgetType === event.widgetType)
            .forEach(pubSub => pubSub.handle(event));
    }

    getCartItems() {
        return this.cartItems;
    }

    linkItems(sourceId, targetId) {
        const source = this.cartItems.find(item => item.id === sourceId);
        const target = this.cartItems.find(item => item.id === targetId);
        source.relatedItemsIds.push(targetId);
        target.relatedItemsIds.push(sourceId);
        const targetPubSub = this.widgetPubSubs.find(pubSub => pubSub.id === targetId);
        targetPubSub.handle(this._shoppingCartEvent({payload: {id: sourceId}, eventType: 'LINK'}));
        console.log('emit', 'LINK', ' | ', sourceId, ' | ', JSON.stringify({id: sourceId}, ' | on target: ', targetId));
    }

    exclusiveLinkItems(sourceId, targetId) {
        const source = this.cartItems.find(item => item.id === sourceId);
        const target = this.cartItems.find(item => item.id === targetId);
        const targetPubSub = this.widgetPubSubs.find(pubSub => pubSub.id === targetId);

        this.cartItems
            .filter(
                item => item.id !== sourceId && item.type === source.type && target.relatedItemsIds.includes(item.id)
            )
            .forEach(item => {
                item.relatedItemsIds = item.relatedItemsIds.filter(id => id !== targetId);
                const itemPubSub = this.widgetPubSubs.find(pubSub => pubSub.id === item.id);
                itemPubSub.handle(this._shoppingCartEvent({payload: {id: targetId}, eventType: 'UNLINK'}));

                target.relatedItemsIds = target.relatedItemsIds.filter(id => id !== item.id);
                targetPubSub.handle(this._shoppingCartEvent({payload: {id: item.id}, eventType: 'UNLINK'}));
            });
        source.relatedItemsIds.push(targetId);
        target.relatedItemsIds.push(sourceId);
        targetPubSub.handle(this._shoppingCartEvent({payload: {id: sourceId}, eventType: 'LINK'}));
    }

    unlinkItems(sourceId, targetId) {
        const source = this.cartItems.find(item => item.id === sourceId);
        const target = this.cartItems.find(item => item.id === targetId);
        if (!target) return;
        source.relatedItemsIds = source.relatedItemsIds.filter(id => id !== targetId);
        target.relatedItemsIds = target.relatedItemsIds.filter(id => id !== sourceId);
        const targetPubSub = this.widgetPubSubs.find(pubSub => pubSub.id === targetId);
        targetPubSub.handle(this._shoppingCartEvent({payload: {id: sourceId}, eventType: 'UNLINK'}));
        console.log(
            'emit',
            'UNLINK',
            ' | ',
            sourceId,
            ' | ',
            JSON.stringify({id: sourceId}, ' | on target: ', targetId)
        );
    }
}

export default Bus;
