import _ from 'lodash';

class PubSub {
    constructor(Bus, widgetType, id) {
        this.widgetType = widgetType;
        this.id = id;
        this.bus = Bus;
        this.listeners = {};
    }

    emit(eventType, payload) {
        this.bus.emit({eventType, payload, widgetId: this.id, widgetType: this.widgetType});
    }

    removeCartItem() {
        this.bus.removeCartItem(this.id);
    }

    broadcastForSibblings(eventType, payload) {
        this.bus.broadcastForSibblings({eventType, payload, widgetId: this.id, widgetType: this.widgetType});
    }

    getOwnState() {
        return _.cloneDeep(this.bus.getCartItems().find(item => item.id === this.id).state);
    }

    getCartItemState(id) {
        return _.cloneDeep(this.bus.getCartItems().find(item => item.id === id).state);
    }

    getCartItemsMetadata() {
        return this.bus.getCartItems().map(({state, ...rest}) => ({...rest}));
    }

    linkWithItem(id) {
        this.bus.linkItems(this.id, id);
    }

    unlinkFromItem(id) {
        this.bus.unlinkItems(this.id, id);
    }

    exclusiveLink(id) {
      this.bus.exclusiveLinkItems(this.id, id)
    }

    subscribe(eventType, listener) {
        this.listeners[eventType] = listener;
    }

    unsubscribe(eventType) {
        delete this.listeners[eventType];
    }

    handle(event) {
        if (typeof this.listeners[event.eventType] === 'function') {
            this.listeners[event.eventType](event);
        }
    }
}

export default PubSub;
