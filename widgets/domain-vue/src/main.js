// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './Domain';

Vue.config.productionTip = false;

/* eslint-disable no-new */
function Domain(PubSub, elementId) {
    new Vue({
        el: `#${elementId}`,
        data() {
          return {PubSub}
        },
        template: `<div id="${elementId}"><App :PubSub="PubSub"/></div>`,
        components: {App},
    });
}

window.ncWidgets = window.ncWidgets || {};
window.ncWidgets.Domain = Domain;
