import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

function PrivateEmail(PubSub, id) {
  ReactDOM.render(<App PubSub={PubSub} />, document.getElementById(id));
}

window.ncWidgets = window.ncWidgets || {};
window.ncWidgets.PrivateEmail = PrivateEmail;
