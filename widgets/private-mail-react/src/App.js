import React, {Component} from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.PubSub = props.PubSub;
    this.state = {...props.PubSub.getOwnState(), bulk: false};

    this.state.domains = this.PubSub.getCartItemsMetadata()
      .filter(item => item.type === 'domain')
      .map(item => ({...this.PubSub.getCartItemState(item.id), id: item.id}));

    this.listener = e => {
      console.log('Private Email', this.PubSub.id, 'received:', JSON.stringify(e));
      switch (e.eventType) {
        case 'UNLINK': {
          return this.setState({assignedDomainId: '-1'})
        }
        case 'CHANGE': {
          return this.setState({[e.payload.path]: e.payload.value})
        }
      }
    };

    this.PubSub.subscribe('CHANGE', this.listener);
    this.PubSub.subscribe('LINK', this.listener);
    this.PubSub.subscribe('UNLINK', this.listener);

    this.formatPrice = price => (price > 0 ? `$${Math.floor(price / 100)}.${Math.floor(price % 100)}` : 'FREE');
    this.onAutorenewChange = e => {
      this.setState({autorenew: e.target.checked});
      if (this.state.bulk) {
        return this.PubSub.broadcastForSibblings('CHANGE', {path: 'autorenew', value: e.target.checked});
      }
      this.PubSub.emit('CHANGE', {path: 'autorenew', value: e.target.checked})
    };

    this.onRegistrationChange = e => {
      this.setState({registration: e.target.value});
      if (this.state.bulk) {
        return this.PubSub.broadcastForSibblings('CHANGE', {path: 'registration', value: e.target.value});
      }
      this.PubSub.emit('CHANGE', {path: 'registration', value: e.target.value})
    };

    this.onPlanChange = e => {
      this.setState({plan: e.target.value});
      if (this.state.bulk) {
        return this.PubSub.broadcastForSibblings('CHANGE', {path: 'plan', value: e.target.value});
      }
      this.PubSub.emit('CHANGE', {path: 'plan', value: e.target.value})
    };

    this.assignToDomain = e => {
      this.setState({assignedDomainId: e.target.value});
      if (e.target.value === '-1') {
        this.PubSub.unlinkFromItem(Number(this.state.assignedDomainId));
        return
      }
      this.PubSub.unlinkFromItem(Number(this.state.assignedDomainId));
      this.PubSub.exclusiveLink(Number(e.target.value));
    };

    this.switchBulk = e => {
      this.setState({bulk: e.target.checked});
    };

    this.removeCartItem = () => {
      this.PubSub.removeCartItem();
    };

    this.onActiveChange = e => {
      this.setState({active: e.target.checked});
      if (this.state.bulk) {
        return this.PubSub.broadcastForSibblings('CHANGE', {path: 'active', value: e.target.checked});
      }
      this.PubSub.emit('CHANGE', {path: 'active', value: e.target.checked})
    }
  }

  render() {
    return (
      <section className="base-dr">
        <h1 className="feature-name">Private Email</h1>
        <button className="remove-btn" onClick={this.removeCartItem}>X</button>
        <p className="dns-price">Price: {this.formatPrice(Number(this.state.registration) * this.state.price)}</p>
        <p className="dns-price">
          <label>
            Active:
            <input type="checkbox" checked={this.state.active} onChange={this.onActiveChange}/>
          </label>
        </p>
        <section className="in-bundle">
          {!this.state.active && <div className="overlay"/>}
          <section className="main-box">
            <select className="dns-select" value={this.state.registration} onChange={this.onRegistrationChange}>
              <option value="1">1 Year</option>
              <option value="2">2 Years</option>
              <option value="3">3 Years</option>
              <option value="4">4 Years</option>
              <option value="5">5 Years</option>
            </select>
            <span className="dns-autorenew-option"><span className="autorenew-option-title">Auto-renew</span>
              <input type="checkbox" checked={this.state.autorenew} onChange={this.onAutorenewChange}/></span>
            <select className="dns-select" onChange={this.assignToDomain} value={this.state.assignedDomainId}>
              <option value="-1">Left Unassigned</option>
              {this.state.domains.map(domain => <option key={domain.id} value={domain.id}>Assign
                to {`${domain.name}.${domain.tld}`}</option>)}
            </select>
            <span className="dns-autorenew-option"><span className="autorenew-option-title">Make Bulk Actions</span>
              <input type="checkbox" checked={this.state.bulk} onChange={this.switchBulk}/>
            </span>
            <label> Pricing Plan:&nbsp;
              <select className="dns-select" value={this.state.plan} onChange={this.onPlanChange}>
                <option value="basic">Basic</option>
                <option value="business">Business</option>
                <option value="luxury">Luxury</option>
              </select>
            </label>
          </section>
        </section>
      </section>
    );
  }
}

export default App;
